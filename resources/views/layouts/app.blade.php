<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Bryan Tom') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/startup-materialize.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<body>
	@include('inc.nav')

            @yield('content')
	{{-- @include('inc.footer') --}}
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!--	    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>-->
		<script src="{{ asset('js/materialize.min.js') }}"></script>

		<script src="{{asset('https://maps.googleapis.com/maps/api/js?key-YOUR_API_KEY_HERE')}}"></script>
		<script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
		<script src="{{ asset('js/masonry.pkgd.min.js') }}"></script>
		<script src="{{ asset('js/TweenMax.min.js') }}"></script>
		<script src="{{ asset('js/ScrollMagic.min.js') }}"></script>
		<script src="{{ asset('js/animation.gsap.min.js') }}"></script>

		<!--Initialization script -->
		<script src="{{ asset('js/startup.js') }}"></script>
		<script src="{{ asset('js/init.js') }}"></script>
		<script src="{{ asset('js/customjs.js') }}"></script>
</body>
</html>
