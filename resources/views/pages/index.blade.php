@extends('layouts.app')
	@section('content')
    <div class="title-transition section white valign-wrapper">
      <div class="row valign">
        <div class=" blog-title col s8 offset-s2 m6 offset-m3 blog-title fade-transition">
			<div class="row hide-on-med-and-down">
				<span class=" date ">Bryan</span>
				<div class="  asdf valign-wrapper">
					<h2 id="T">T</h2>
					<h2 id="O">O</h2>
					<h2 id="M">M</h2>
				</div>
				<span id="title-large" class="date">Backend Engineer | Web Dev</span>
			</div>
			<div class="row hide-on-large-only">
				<hr>
				<div class="col s12 m12">
					<span class="s12 date m12 ">Bryan</span>
				<hr>
					<div class="asdf valign-wrapper">
                        <h2 id="T">T</h2>
						<h2 id="O">O</h2>
						<h2 id="M">M</h2>
					</div>
				</div>
					<span id="title" class="date">Backend Engineer</span>
			</div>


        </div>
      </div>
    </div>
    <div id="aboutme" class="header full-height horizontal-half-transition">
        <div class="background  valign-wrapper white">
            <div class="block left-block center col s12 m6 white">
                <h1 id="education">Education</h1>
                <ul>
                    <li>
                        <h4>California University State Long Beach</h6>
                        <p>Master's in Computer Science</p>
                    </li>
                    <li>
                        <h4>UC Berkeley</h4>
                        <p>English B.A.</p>
                    </li>
                    <li>
                        <h4>Glendale Community College</h4>
                        <p>Computer Programmer Certificate</p>
                    </li>
                </ul>
            </div>

            <div class="block right-block center col s12 m6 white">
                <h1 id="aboutme">About Me</h1>
                <p class="background-card">After completing my English B.A. and after over a year working at a startup in the Bay Area, I made the decision to give a computer science Master's program a shot since I have always had a fascination with technology. Since making that life altering decision, I've never looked back. With over 5 years as a developer, I still find myself continually seeking out and researching new and better ways to implement technology and improve upon older projects. One sector that particularly appeals to me and that I have a passion for is in higher education. That being said, I really enjoyed my opportunity to continually work with Mackey Creative Lab on a project that helps benefit students by empowering them with the ability to navigate their way through higher education. Interested in working together or want to learn more? Feel free to reach out!</p>
            </div>
        </div>

        {{-- <div class="phone-preview-sizer">
                <h1>Second!</h1>
                <span class="tagline">Second tagline with more information.</span>
        </div> --}}
        <div class="horizontal-half-wrapper right-side active">
                <div class="header-background black"></div>
                <div class="header-wrapper row valign-wrapper">
                  <div class="col s12 m8 offset-m2 valign">
                      <h1>Hello!</h1>
                        <p style="color: #42c4ac;">Thanks for stopping in my portfolio, feel free to take a look around!</p>
                         <!-- or grab a copy of my resume below!</p> -->
                        <!-- <a href="/public/resume/resume(bryan_tom).pdf" download>Full Resume Download (pdf)</a> -->
                  </div>
                </div>
        </div>
        <div class="horizontal-half-wrapper active">
                <div class="header-background black"></div>
                <div class="header-wrapper row valign-wrapper">
                  <div class="col s12 m8 offset-m2 valign">
                                <img id="bryan_image" src="{{ asset('svg/mackey-bryan.png') }}" alt="">
                    <span class="tagline">Bryan Tom</span>

                  </div>
                </div>
        </div>
        <!-- Can Repeat with horizontal-half-wrapper -->
      </div>

{{-- ab3b3b --}}
<div id="projects" class="section white full-height">
    <div class="row">
            <h2  class="col s8 offset-s2 l4 offset-l5">#Portfolio</h2>
        </div>
    </div>
    <div class="clients">
        <div class="row">
            <div class="col s12 m6 l6 client black">
                <a href="https://career-snapshot.com/" class="">
                <img class="logo" src="{{ asset('svg/mackey-cl-logo.svg') }}" alt="careersnapshot">
                <span class="title">Career-Snapshot</span>
                <p class="project-summary">Career Snapshot is a web application that utilizes a custom content management system (CMS) that allows universities to display to their students more than 7,000 relevant jobs and career data from the Department of Bureau Labor and Statistics associated with the college's particular university programs and degrees.</p>
                </a>
                <a href="#careersnapshot" class="btn project-info">Project Details</a>
            </div>
            <div class="col s12 m6 l6 client black">
                <a href="https://embedgooglephotos.com">
                    <code style="color: #42c4ac;" class="logo">&lt/Embed Google Photos&gt</code>
                {{-- <img class="logo" src="{{ asset('svg/tom-logo.svg') }}" alt="embedgooglephotos"> --}}
                    <span class="title">Embed Google Photos</span>
                    <p class="project-summary">Embed Google Photos is a web application that enables users to utilize Google Photos as a photo hosting service for their content by allowing them to generate embed codes quickly and easily. This task is accomplished through the implementation of drag and drop functionality and batch processing for photos.</p>
                </a>
                <a href="#embedgooglephotos"class="btn project-info">Project Details</a>
            </div>
        </div>
    </div>
</div>
<div id="careersnapshot" class="header full-height horizontal-half-transition">
        <div class="background black valign-wrapper">
                        <div class="block left-block center col s12 m6">
                            <h1>Technical Specifications</h1>
                            <h4>Languages</h4>
                            <ul>
                                <li>PHP</li>
                                <li>SQL</li>
                                <li>Javascript</li>
                                <li>CSS</li>
                                <li>HTML</li>
                            </ul>
                            <h4>Frameworks & Technologies</h4>
                            <ul>
                                <li>Laravel</li>
                                <li>Bootstrap v4</li>
                            </ul>
                            <h4>System Design</h4>
                            <ul>
                                <li>MVC Architecture</li>
                                <li>LAMP Stack</li>
                            </ul>
                        </div>
                        <div class="block right-block center col s12 m6">
                            <h1 id="">Project Overview</h1>
                            <p>Career Snapshot is a web application that utilizes a custom content management system (CMS) that allows universities to display to their students more than 7,000 relevant jobs and career data from the Department of Bureau Labor and Statistics associated with the college's particular university programs and degrees.</p>
                            <h1 id="">My Role</h1>
                            <p>My role for this project is ongoing, and it has thus far included architechting, implementing, and maintaing the required databases and data for this project. In addition to this, my responsibilites have included implementing the project's workflow, configuring and maintaining the application's servers, accessing data from multiple APIs, architecting and designing the system, and implementing major back-end functionality.</p>
                        </div>
        </div>

        {{-- <div class="phone-preview-sizer">
                <h1>Second!</h1>
                <span class="tagline">Second tagline with more information.</span>
        </div> --}}
        <div class="horizontal-half-wrapper right-side active">
                <div class="header-background white"></div>
                <div class="header-wrapper row valign-wrapper">
                  <div class="col s12 m8 offset-m2 valign">
                      <h1>Career Snapshot</h1>
                        <p class="black"><strong>Developer | System Architect | Backend-Engineer</strong></p>
                  </div>
                </div>
        </div>
        <div class="horizontal-half-wrapper active">
                <div class="header-background white"></div>
                <div class="header-wrapper row valign-wrapper">
                  <div class="col s12 m8 offset-m2 valign">
                                <img  src="{{ asset('svg/mackey-cl-logo.svg') }}" alt="">


                  </div>
                </div>
        </div>
        <!-- Can Repeat with horizontal-half-wrapper -->
    </div>
<div id="embedgooglephotos" class="header full-height horizontal-half-transition">
    <div class="background black valign-wrapper">
                    <div class="block left-block center s6">
                        <h1>Technical Specifications</h1>
                        <h4>Languages</h4>
                        <ul>
                            <li>PHP</li>
                            <li>Javascript</li>
                            <li>CSS</li>
                            <li>HTML</li>
                        </ul>
                        <h4>Frameworks & Technologies</h4>
                        <ul>
                            <li>Laravel</li>
                            <li>Material Design/MaterializeCSS</li>
                        </ul>
                        <h4>System Design</h4>
                        <ul>
                            <li>MVC Architecture</li>
                            <li>LAMP Stack</li>
                        </ul>


                    </div>
                    <div class="block right-block center col s5">
                        <h1>Project Overview</h1>
                        <p>Embed Google Photos is a web application that enables users to utilize Google Photos as a photo hosting service for their content by allowing them to generate embed codes quickly and easily. This task is accomplished through the implementation of drag and drop functionality and batch processing for photos.</p>
                        <h1>My Role</h1>
                        <p>I am responsible for creating and developing this application. It was created on my spare time as a result of my search, which yielded no other viable technologies that accomplished the same level of funcitonality as Embed Google Photos.</p>
                    </div>
    </div>

    {{-- <div class="phone-preview-sizer">
            <h1>Second!</h1>
            <span class="tagline">Second tagline with more information.</span>
    </div> --}}
    <div class="horizontal-half-wrapper right-side active">
            <div class="header-background white"></div>
            <div class="header-wrapper row valign-wrapper">
              <div class="col s12 m8 offset-m2 valign">
                  <h1>Embed Google Photos</h1>
                    <p class="black">Creator | Developer</p>
              </div>
            </div>
    </div>
    <div class="horizontal-half-wrapper active">
            <div class="header-background black"></div>
            <div class="header-wrapper row valign-wrapper">
              <div class="col s12 m8 offset-m2 valign">
                <code style="color: #42c4ac;">&lt/Embed Google Photos&gt</code>
                            {{-- <img  src="{{ asset('svg/tom-logo.svg') }}" alt=""> --}}

              </div>
            </div>
    </div>
    <!-- Can Repeat with horizontal-half-wrapper -->
</div>

<div class="section white full-height">
    <div class="row">
        <div class="col offset-s1 s10">
            <div class="pricing-table">
                <div class="pricing-header center-align">
                    {{-- <i class="icon-plane"></i> --}}
                    <h2 class="hide-on-small-only">Software & Technologies</h2>
                </div>
                <div class="row">
                    <ul class="col s12 m12 l6 pricing-features">
                        <h3 class="col s12 center">CSS</h3>
                        <div class="col s12 center">
                            <li class="pricing-feature"><i class="icon-accept"></i>Bootstrap v4 & v3</li>
                        </div>
                        <div class="col s12 center">
                            <li class="pricing-feature"><i class="icon-accept"></i>Material Design/MaterializeCSS</li>
                        </div>
                    </ul>
                    <ul class="col s12 m12 l6 pricing-features">
                        <h3 class="col s12 center">Javascript</h3>
                        <div class="col s12 center">
                            <li class="pricing-feature"><i class="icon-accept"></i>Angular JS</li>
                        </div>

                        <div class="col s12 center">
                            <li class="pricing-feature"><i class="icon-accept"></i>Ajax</li>
                        </div>

                        <div class="col s12 center">
                            <li class="pricing-feature"><i class="icon-accept"></i>Node.js</li>
                        </div>

                        <div class="col s12 center">
                            <li class="pricing-feature"><i class="icon-accept"></i>Express.js</li>
                        </div>

                        <div class="col s12 center">
                            <li class="pricing-feature"><i class="icon-accept"></i>Vue.js</li>
                        </div>

                        <div class="col s12 center">
                            <li class="pricing-feature"><i class="icon-accept"></i>jQuery</li>
                        </div>

                        <div class="col s12 center">
                            <li class="pricing-feature"><i class="icon-accept"></i>JSON</li>
                        </div>

                    </ul>
                    <ul class="col s12 m12 l6 pricing-features">
                            <h3 class="col s12 center">Version Control Systems</h3>
                            <div class="col s12 center">
                                <li class="pricing-feature"><i class="icon-accept"></i>git</li>
                            </div>

                            <div class="col s12 center">
                                <li class="pricing-feature"><i class="icon-accept"></i>GitHub</li>
                            </div>

                            <div class="col s12 center">
                                <li class="pricing-feature"><i class="icon-accept"></i>Bitbucket</li>
                            </div>

                            <div class="col s12 center">
                                <li class="pricing-feature"><i class="icon-accept"></i>Gitlab</li>
                            </div>

                        </ul>
                        <ul class="col s12 m12 l6 pricing-features">
                                <h3 class="col s12 center">Database Technologies</h3>
                                <div class="col s12 center">
                                    <li class="pricing-feature"><i class="icon-accept"></i>MySQL</li>
                                </div>

                                <div class="col s12 center">
                                    <li class="pricing-feature"><i class="icon-accept"></i>NoSql</li>
                                </div>

                                <div class="col s12 center">
                                    <li class="pricing-feature"><i class="icon-accept"></i>MongoDB</li>
                                </div>

                                <div class="col s12 center">
                                    <li class="pricing-feature"><i class="icon-accept"></i>Eloquent ORM</li>
                                </div>
                                <div class="col s12 center">
                                    <li class="pricing-feature"><i class="icon-accept"></i>PHPMyAdmin</li>
                                </div>

                            </ul>
                            <ul class="col s12 m12 l6 pricing-features">
                                    <h3 class="col s12 center">Languages</h3>
                                    <div class="col s12 center">
                                        <li class="pricing-feature"><i class="icon-accept"></i>PHP</li>
                                    </div>

                                    <div class="col s12 center">
                                        <li class="pricing-feature"><i class="icon-accept"></i>Python</li>
                                    </div>

                                    <div class="col s12 center">
                                        <li class="pricing-feature"><i class="icon-accept"></i>Javascript</li>
                                    </div>

                                    <div class="col s12 center">
                                        <li class="pricing-feature"><i class="icon-accept"></i>HTML5</li>
                                    </div>

                                    <div class="col s12 center">
                                        <li class="pricing-feature"><i class="icon-accept"></i>SQL</li>
                                    </div>

                                    <div class="col s12 center">
                                        <li class="pricing-feature"><i class="icon-accept"></i>Java</li>
                                    </div>

                                </ul>
                                <ul class="col s12 m12 l6 pricing-features">
                                        <h3 class="col s12 center">APIs</h3>
                                        <div class="col s12 center">
                                            <li class="pricing-feature"><i class="icon-accept"></i>Stripe API</li>
                                        </div>
                                        <div class="col s12 center">
                                            <li class="pricing-feature"><i class="icon-accept"></i>Square Payment APIs</li>
                                        </div>

                                        <div class="col s12 center">
                                            <li class="pricing-feature"><i class="icon-accept"></i>Bureau of Labor Statistics API</li>
                                        </div>
                                        {{-- <div class="col s12 m7">
                                            <li class="pricing-feature"><i class="tiny material-icons">chevron_left</i>
                                                <i class="tiny material-icons">star</i>
                                                <i class="tiny material-icons">star</i>
                                                <i class="tiny material-icons">star</i>
                                                <i class="tiny material-icons">star_border</i>
                                                <i class="tiny material-icons">star_border</i>
                                            <i class="tiny material-icons">chevron_right</i></li>
                                        </div> --}}

                                    </ul>


                </div>
            </div>
        </div>
    </div>
</div>
	@endsection
