    <!-- Navbar -->
    <nav class="navbar navbar-solid-transition">
      <div class="nav-wrapper">
        {{-- <a href="#embed" class="brand-logo"><img src="{{ asset('svg/tom-logo.svg') }}" alt="embedgooglephotos"></a> --}}
        <ul id="nav-mobile" class="right hide-on-med-and-down">
          <li><a href="#aboutme">Profile</a></li>
          <li><a href="#projects">Portfolio</a></li>
          <li><a href="#education">Education</a></li>
          <li><a href="#about">About</a></li>
<!--          <li><a href="#contact">Contact</a></li>-->
        </ul>


        <a href="#" data-activates="slide-out" class="button-collapse right"><i class="material-icons ">menu</i></a>
      </div>
    </nav>
    <ul id="slide-out" class="side-nav">
      </li>
          <li><a href="#aboutme">Profile</a></li>
          <li><a href="#projects">Portfolio</a></li>
          <li><a href="#education">Education</a></li>
          <li><a href="#about">About</a></li>
<!--          <li><a href="#contact">Contact</a></li>-->
    </ul>
