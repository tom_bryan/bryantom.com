$( document ).ready(function() {

	var swapStatus = 0;
	/*Copy Embed Code*/
	$('body').on('click', '.copy', function(){
              /* Get the text field */
//		console.log("HIT");
			  var x = $(this).attr('name');
              var copyText = document.getElementById(x);
              /* Select the text field */
              copyText.select();
              /* Copy the text inside the text field */
              document.execCommand("copy");
			 var $toastSuccess = $('<span class="toastify"><p>Copied embed to clipboard: ' + copyText.value +'<button class="toast-action btn-flat right"><i class="material-icons">clear</i></button><p></span>')
			 Materialize.toast($toastSuccess, 5000, 'rounded success');

	});
	/*Add New Item w. Delegated function*/
		var counter = 1;
	$('#form-wrapper').on('click', '.add_embed', function() {
		counter++;
		if(counter <= 5){
			$('#add').before('<div class="item row"><div class="input-field col s12 m5"><input id="photo['+counter+'][url]" name="photo['+counter+'][url]" type="text"><label for="photo['+counter+'][url]" name="photo.'+counter+'.url">Image URL</label><span class="errors"  name="photo.'+counter+'.url"></span></div><div class="input-field col s6 m2"><input id="photo['+counter+'][width]" name="photo['+counter+'][width]"  type="text"><label for="photo['+counter+'][width]" name="photo.'+counter+'.width">Width (px)</label><span class="errors"  name="photo.'+counter+'.width"></span></div><div class="input-field col s6 m2"><input id="photo['+counter+'][height]" name="photo['+counter+'][height]"  type="text"><label for="photo['+counter+'][height]" name="photo.'+counter+'.height">Height (px)</label><span class="errors"  name="photo.'+counter+'.height"></span></div><div class="input-field col s6 m2"><input type="checkbox" class="filled-in"  name="photo['+counter+'][original]" id="photo['+counter+'][original]" checked="checked"/><label for="photo['+counter+'][original]" name="photo.'+counter+'.original">Use Original Size</label><span class="errors" name="photo.'+counter+'.original"></span></div><div class="input-field col s6 m1"><a class="waves-effect waves-light btn left add_embed"><i class="material-icons">add</i></a></div>');
		}
	});
	
	$('body').on('click', '.toast-action', function(event){
		$('.toast').fadeOut("slow","linear", function(){
			$('.toast').remove();
		});
	});
	/*clipboard form*/
	$('#embed').on('click', '#clipboardButton', function(event){
		event.preventDefault();
		/*clear embeds*/
		/*Swap Section*/
		$('#swap2').fadeOut("slow", "linear", function(){
			$('#swap1').fadeIn("slow", "linear");
		$("#clipboard").html(" ");
		});
	
		/*End Swap Section*/

	});
				   

	//Ajax Submission
	$('#embed').on('submit', '#embeds_form', function(event){
	  event.preventDefault();
		document.getElementById("generateButton").disabled=true;
		console.log("AJAX HIT");
	  //declare CSRF variable for dynamic form use
	  var formData = $(this).closest('form').serializeArray();
      console.log(formData);
		$.ajax({
			type: "POST",
			url: "/",
			data: formData,
			success: function(data){
				$("span.errors").html(" ");
				/*Load URLS from ajax request*/
					$('#clipboard').append('<div id="add_clipboard" class="input-field col s12"><button id="clipboardButton" class="waves-effect waves-light btn-large">Generate More Embeds</button></div>');
				var instance = 1
				console.log(data);
				$(data.embeds).each(function(){
					instance++;
					/*Create Item Variable*/
					$('#add_clipboard').before('<div class="item row"><div class="input-field col s11 m7 offset-m2"><input type="text" class="materialize-textarea" value="'+this.toString()+'" id="embed['+instance+']"><label for="embed['+instance+']">Embeddable URL:</label></div><div class="input-field col s1 m1"><a name="embed['+instance+']" class="copy waves-effect waves-light btn"><i class="material-icons">content_copy</i></a></div></div>');
					console.log(this.toString());
				});
				/*End Load URLS from ajax request*/
				
				/*Swap Section*/
				$('#swap1').fadeOut("slow", "linear", function(){
					$('#swap2').fadeIn("slow", "linear");
					document.getElementById("generateButton").disabled=false;
					 var $toastSuccess = $('<span class="toastify"><p>Conversion Successful!<button class="toast-action btn-flat right"><i class="material-icons">clear</i></button><p></span>')
					 Materialize.toast($toastSuccess, 5000, 'rounded success');
									console.log("CSRF = " + csrf_js_var);

					/*clear and print new form*/
					$('#embeds_form').html('<input name="_token" value="'+csrf_js_var+'" type="hidden"><div class="col s12"><h2 class="section-title">Drag and Drop Image below</h2></div><div class="row"><div class="item row"><div class="input-field col s12 m5"><input id="photo[1][url]" name="photo[1][url]" type="text" ><label for="photo[1][url]" name="photo.1.url">Image URL</label><span class="errors"  name="photo.1.url"></span></div><div class="input-field col s6 m2"><input id="photo[1][width]" name="photo[1][width]"  type="text"><label for="photo[1][width]" name="photo.1.width" data-error="">Width (px)</label><span class="errors" name="photo.1.width"></span></div><div class="input-field col s6 m2"><input id="photo[1][height]" name="photo[1][height]"  type="text"><label for="photo[1][height]" name="photo.1.height" data-error="">Height (px)</label><span class="errors"  name="photo.1.height"></span></div><div class="input-field col s6 m2"><input type="checkbox" class="filled-in" id="photo[1][original]" name="photo[1][original]" checked="checked"/><label for="photo[1][original]" name="photo.1.original" data-error="">Use Original Size</label><span class="errors"  name="photo.1.original"></span></div><div class="input-field col s6 m1"><a class="waves-effect waves-light btn left add_embed"><i class="material-icons">add</i></a></div></div><div id="add" class="input-field col s12"><button id="generateButton"  type="submit" class="waves-effect waves-light btn-large">Generate Embed</button></div></div>');
					counter = 1;
				} );
//				document.getElementById("embeds_form").reset();
				/*End Swap Section*/
			},
			error: function(data){
				$("span.errors").html(" ");
		document.getElementById("generateButton").disabled=false;

			 if( data.status === 500 ) {
				 /*#Server Failed Toast*/
				 var $toast500 = $('<span class="toastify 500"><p>An image encountered a 500 error. Please make sure all your links are valid.<button class="toast-action btn-flat center valign-wrapper"><i class="material-icons">clear</i></button><p></span>')
				 Materialize.toast($toast500, 5000, 'rounded fivehundred');
			 }
			 if( data.status === 422 ) {
				 var errors = $.parseJSON(data.responseText);
				 //add errors
				 console.log(data);
				 $.each(errors.errors, function (key, value) {
					$("span[name='"+key+"']").html("<i class='material-icons '>error_outline</i> "+value.toString());
					console.log(key + value);
				});
			 }}
		});
	});	
});
